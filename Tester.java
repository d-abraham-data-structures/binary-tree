import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class Tester {
	public static void main(String[] args) {
		BufferedReader in ;
		BinaryTree<String> tree;
		try {
			in = new BufferedReader(new FileReader("test.txt"));
			tree= BinaryTree.readBinaryTree(in);
			System.out.println(tree.toString());
			System.out.println("Number of leaves "+tree.leaves(tree.root));
			System.out.println("Tree height "+tree.height(tree.root));
			tree.defoliate(tree.root);
			System.out.println(tree.toString());
			System.out.println("Number of leaves "+tree.leaves(tree.root));
			System.out.println("Tree height "+tree.height(tree.root));
		}
		catch(IOException e) {
			System.out.print(e.toString());
		}
	}
}
